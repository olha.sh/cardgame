
import React, {useEffect, useState, useMemo} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Modal,
  TouchableOpacity,
} from 'react-native';
import MemoryGame from './component/memoryGame'
import {FindBlock} from './component/find_block'


  const cards= [
    {
      src: require('./assets/card1.png'),
      id: 0,
      isOpen: false
      }, 
    {
      src: require('./assets/card2.png'),
      id: 1,
      isOpen: false
    },
    {
      src: require('./assets/card3.png'),
      id: 2,
      isOpen: false
    },
    {
      src: require('./assets/card4.png'),
       id: 3,
       isOpen: false
    },
    {
      src: require('./assets/card5.png'),
      id: 4,
      isOpen: false
    }, 
    {
      src: require('./assets/card6.png'), 
      id: 5,
      isOpen: false
    },
    {
      src: require('./assets/card7.png'),
       id: 6,
       isOpen: false
    },
    {
      src: require('./assets/card8.png'),
       id: 7,
       isOpen: false
    },
  ];

function App() {
  const [cardsState, setCardsState] = useState(cards);
  const [clicked, setClicked] = useState(false)
  const [openCards, setOpenCards] = useState(false)
  const [findCard, setFindCard] = useState(randomIndex(cards));
  const [showCard, setShowCard] = useState (false);
  const [modalVisible, setModalVisible] = useState(false);

  const [score, setScore] = useState(0);


 function handlOpenCard(){
  
  const newArr = [];
  setCardsState(shuffleArray(cardsState))
  cardsState.forEach((item, index) => {
    newArr[index] = item;
    newArr[index].isOpen = true;
    console.log('handleOpen' + newArr[index].id)
  });

  setCardsState(newArr)
  return newArr
}

function handlCloseCard(){
  const newArr = [];
  cardsState.forEach((item, index) => {
    newArr[index] = item;
    newArr[index].isOpen = false;
    console.log('handleClose' + newArr[index].id)
  });
  setCardsState(newArr)
  setClicked(true)
  return newArr
}

function handleChooseCard (id) {
  const newArr = [];
  cardsState.forEach((item, index) => {
    newArr[index] = item;
    if( id === newArr[index].id){
      newArr[index].isOpen = true;
      setTimeout(()=> {
        newArr[index].isOpen = false;
      }, 1000)
      if (id === findCard.id){
        let value = score + 2;
        setScore(value)
        setShowCard(false)
        
      } else {
        setTimeout(()=>{
          setModalVisible(true)
        }, 1000)
        
        setScore(0)
        setClicked(false)
      }
    }
    console.log('handleChoose ' +  newArr[index].id)

  });
  setCardsState(newArr)
  return newArr
}

function shuffleArray (arr) {
  let newArr = arr.sort(() => Math.random() - 0.5)
  newArr.forEach(el=> console.log(`el.id ` +el.id))
  return newArr;
};
function randomIndex(arr){
  const index = Math.floor(Math.random() * arr.length)
  return arr[index];
}


function closeModal(){
  setModalVisible(false)
  setShowCard(false)
}


useEffect(()=>{
  if(modalVisible === false){
  const openTimer =  setTimeout(()=>{
      handlOpenCard()
    }, 2000)
    
   const closeTimer = setTimeout(()=>{
      setFindCard(randomIndex(cards))
      setShowCard(true)
    }, 5000)
  
 const openCard = setTimeout(()=>{
      handlCloseCard()
    }, 5000)
    
    return () => clearTimeout(openTimer, closeTimer, openCard)
  }
  
}, [ score, modalVisible])



  return (
     <ImageBackground 
        source={require('./assets/bg.png')} 
        style={styles.fonImage}
     >
       <Modal
        transparent={true}
        visible={modalVisible}
       >
            <TouchableOpacity 
              style={styles.centeredView}
              onPressOut={closeModal}>
              <ImageBackground
                style={styles.modalView}
                source={require('./assets/modal.png')}
                resizeMode='contain'
                >
                <Text style={ styles.modalText1}>
                  GAME OVER
                </Text>  
                <Text style={ styles.modalText2}>
                  Score {score}
                </Text>
                <Text style={styles.modalText3}>
                  Tap to continue
                </Text>
              </ImageBackground>
              
            </TouchableOpacity>
       </Modal>
        <View style={styles.container}>
          <ImageBackground 
            source={require('./assets/score.png')} 
            style={styles.header}>
            <Text style={styles.headerText}>
              Score: {score}
            </Text>
          </ImageBackground>
          <MemoryGame
            cards={cardsState}
            canClick={clicked}
            handleOpen={(value)=> setOpenCards(!value)}
            handleCard={handleChooseCard}
          />
        </View>
        {showCard && <FindBlock src={findCard.src}/>} 
     </ImageBackground>      
  );
};

const styles = StyleSheet.create({
  fonImage: {
    height: "100%",
    width: "100%",
    resizeMode: "cover",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(0, 0, 0, 0.7)'
  },
  modalView: {
    marginTop: 0,
    alignItems: "center",
    shadowColor: "#000",
    width: 350,
    height: 180,
    justifyContent: 'space-evenly',
    padding: 20,
   
    
  },
  modalText1: {
    fontSize: 20,
    color: "#fcff03",
    textTransform: 'uppercase',
    fontWeight: "bold",
  },
  modalText2: {
    fontSize: 22,
    color: "#ffff",
    textTransform: 'uppercase',
    fontWeight: "bold",
  },
  modalText3: {
    fontSize: 13,
    color: "#01131f",
    textTransform: 'uppercase',
    fontWeight: "bold",
  },
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
   
  },
  header:{
    width: 300,
    height:70,
    resizeMode: 'cover',
    marginBottom: 30,
    marginTop: 20
  },
  headerText: {
    textAlign: 'center',
    fontSize: 23,
    padding: 18,
    color: "#fcff03",
    textTransform: 'uppercase',
    fontWeight: "bold",
    fontFamily: "Cochin"
  },
});

export default App;
