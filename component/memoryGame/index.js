import React from 'react';
import {StyleSheet, ImageBackground, View, Image} from 'react-native'
import Card from '../card'
function MemoryGame({cards,handleCard, canClick}){
    return(
    <ImageBackground 
        source={require('../../assets/container.png')} 
        style={styles.fonContainer}
        resizeMode='contain'
    >
        <View style={styles.wrapperCard}>
        {cards.map((item)=> {
            return  (
        <Card 
            click={canClick}
            src={item.src} 
            key={item.id}
            openCard={item.isOpen}
            idKey={item.id}
            chooseCard={handleCard}
        />
        )
    })} 
        </View> 
    </ImageBackground>
    )
}

const styles = StyleSheet.create({

    
    fonContainer: {
        width: 400,
        height: 300,
        position: 'relative',
        justifyContent: 'center',
      },
    wrapperCard: {
        margin: 30,
        position: 'absolute',
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    }
})


export default MemoryGame
