import React, { useEffect} from 'react';
import {StyleSheet, View, Image, TouchableOpacity} from 'react-native'


function Card({src, openCard, idKey, chooseCard, click}){
 
   function handlChooseCard(id){
       if(click){
          chooseCard(idKey) 
       }
    
   }
    useEffect(()=>{
       console.log('IDKEY CARD '+ idKey)
    }, [openCard])
    return(
        <View
            style={styles.card} 
            key={idKey} 
        >
            {openCard ? (
                <TouchableOpacity    onPress={(idKey)=>handlChooseCard(idKey)}  key={idKey} >
                    <Image source={src} 
                    style={styles.fontImg}
                    /> 
                </TouchableOpacity>
                    ) : 
                (
                    <TouchableOpacity onPress={()=>chooseCard(idKey)} >
                        <Image  source={require('../../assets/back.png')} 
                        style={ openCard ? styles.flipBack : styles.backImg}
                    /> 
                    </TouchableOpacity>
                )}
        </View>   
        )
}
const styles = StyleSheet.create({
    card:{
        width: 80,
        height: 80,
        marginBottom: 6,
    },
    flipBack :{
        display: 'none'
    },
    fontImg: {
        width: 70,
        height: 70,
        margin: 3,
    },
    backImg: {
        width: 80,
        height: 80,
      }
})




export default Card